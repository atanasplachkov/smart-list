"use strict";

var Iterator = function (data, previous, next, parent) {
	this.data = data;

	this._previous = previous;
	this._next = next;
	this._parent = parent;
};

Iterator.prototype.next = function () {
	return this._next;
};

Iterator.prototype.previous = function () {
	return this._previous;
};

Iterator.prototype.hasNext = function () {
	return (this._next ? !!this._next : false);
};

Iterator.prototype.hasPrevious = function () {
	return (this._previous ? !!this._previous : false);
};

Iterator.prototype.drop = function () {
	if (this._previous) {
		this._previous._next = this._next;
	}
	if (this._next) {
		this._next._previous = this._previous;
	}

	var parent = this._parent;
	parent._length--;

	if (parent._first === this) {
		parent._first = this._next;
	}
	if (parent._last === this) {
		parent._last = this._previous;
	}
};

module.exports = Iterator;